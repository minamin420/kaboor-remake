﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterLoad : MonoBehaviour
{
    [SerializeField]
    private List<GameObject> player;
    [SerializeField]
    private int selectionIndex = 0;
    // Start is called before the first frame update
    void Start()
    {
        selectionIndex = PlayerPrefs.GetInt("SelectedChara");

        player = new List<GameObject>();
        foreach (Transform t in transform)
        {
            player.Add(t.gameObject);
            t.gameObject.SetActive(false);
        }
        player[selectionIndex].SetActive(true);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
