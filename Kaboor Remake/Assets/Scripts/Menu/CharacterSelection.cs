﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class CharacterSelection : MonoBehaviour
{
    [SerializeField]
    int selectedChara = 0;
    public void SelectChar(int index)
    {
        Debug.Log("Char " + index + " Selected");
        selectedChara = index;
        PlayerPrefs.SetInt("SelectedChara", (selectedChara));
    }

    public void PlayGame()
    {
        SceneManager.LoadScene("Level");
    }
}
